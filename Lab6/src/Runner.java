import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Runner{


	public static void main(String[] args){
		//read file
		Queue<Jobs> test= loadtxtTest();
		//execute the test
		runWithTypeQueue(test);






	}

	public static <E> void runWithTypeQueue(Queue<Jobs> elements) {
		Queue<Jobs> inProgress= new ArrayQueue<Jobs>();
		Queue<Jobs> terminatedJobs=new SLLQueue<Jobs>();

		int time=0;
		float promedio;
		float acumulaciondevalores=0;

		do {
			//System.out.println("Lo realizado en tiempo "+time);

			//adding new Jobs
			while (!elements.isEmpty() && (elements.first()).getArrivalTime()==time) {
				//Adding a Job to the processing Queue
				Jobs adding=elements.dequeue();
				inProgress.enqueue(adding);

				//System.out.println("Se a�adio trabajo");


				//To use this test is necesarry to change the declaration to ArrayQueue<E> because
				//such method was created to test the ArrayQueue 
				//System.out.println("cap " + inProgress.lenght() +"size " +inProgress.size() );
			}

			//Doing job in the processing queue
			if(!inProgress.isEmpty()) {

				Jobs cur = inProgress.dequeue();
				cur.setTimeNeeded(cur.getTimeNeeded()-1);
				if(cur.getTimeNeeded()==0) {
					cur.setLeavingTime(time);
					promedio =cur.getLeavingTime()-cur.getArrivalTime()+1;
					acumulaciondevalores+=promedio;
					terminatedJobs.enqueue(cur);

					//System.out.println("Se termino un trabajo ");

					//To use this test is necesarry to change the declaration to ArrayQueue<E> because
					//such method was created to test the ArrayQueue 
					//System.out.println("cap " + inProgress.lenght() +"size " +inProgress.size() );
				}
				else {
					inProgress.enqueue(cur);

					//System.out.println("Se hizo un trabajo pero todavia le queda "+ cur.getTimeNeeded());
				}
			}
			time++;

		}while(!inProgress.isEmpty() || !elements.isEmpty());
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		promedio=acumulaciondevalores/terminatedJobs.size();
		System.out.println("Average Time in System is: "+ numberFormat.format(promedio));
	}



	public static Queue<Jobs> loadtxtTest(){
		//reading the file
		JFrame frame= new JFrame();
		frame.setSize(600, 500);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setTitle("Select the file with data");

		JFileChooser adds = new JFileChooser();
		adds.setApproveButtonText("Load");
		adds.cancelSelection();
		adds.setVisible(true);
		File f=null;

		String txtFile=null;

		adds.showOpenDialog(frame);
		f = adds.getSelectedFile();
		txtFile = f.getPath();
		String line = "";
		String txtSplitBy = ",";
		String[] save;
		Queue<Jobs> inputQueue= new SLLQueue<Jobs>();

		try (BufferedReader br = new BufferedReader(new FileReader(txtFile))) {
			while ((line = br.readLine()) != null) {
				save = line.split(txtSplitBy);
				int save1=Integer.valueOf(save[0]);
				int save2;
				if(save[1].contains(" "))
					save2=Integer.valueOf(save[1].substring(1));
				else
					save2=Integer.valueOf(save[1]);
				Jobs orde = new Jobs(save1,save2);
				inputQueue.enqueue(orde);}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return inputQueue;
	}
}

