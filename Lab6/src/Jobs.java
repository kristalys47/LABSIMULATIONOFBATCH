/*
 * This class is use to store all the info of a job in just one object.
 */

public class Jobs {
	private int arrivalTime, timeNeeded, leavingTime;

	public Jobs(int arrival, int time) {
		arrivalTime=arrival;
		timeNeeded=time;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getTimeNeeded() {
		return timeNeeded;
	}

	public int getLeavingTime() {
		return leavingTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public void setTimeNeeded(int timeNeeded) {
		this.timeNeeded = timeNeeded;
	}

	public void setLeavingTime(int leavingTime) {
		this.leavingTime = leavingTime;
	}

}